#!/usr/bin/env python

colors = {
   "Default": 39,
   "Black": 30,
   "Red": 31,
   "Green": 32,
   "Yellow": 33,
   "Blue": 34,
   "Magenta": 35,
   "Cyan": 36,
   "Light gray": 37,
   "Dark gray": 90,
   "Light red": 91,
   "Light green": 92,
   "Light yellow": 93,
   "Light blue": 94,
   "Light magenta": 95,
   "Light cyan": 96,
   "White": 97,
   "bg Default": 49,
   "bg Black": 40,
   "bg Red": 41,
   "bg Green": 42,
   "bg Yellow": 43,
   "bg Blue": 44,
   "bg Magenta": 45,
   "bg Cyan": 46,
   "bg Light gray": 47,
   "bg Dark gray": 100,
   "bg Light red": 101,
   "bg Light green": 102,
   "bg Light yellow": 103,
   "bg Light blue": 104,
   "bg Light magenta": 105,
   "bg Light cyan": 106,
   "bg White": 107,
}

def highlight(string, color, **kwargs):
    attrs = [repr(colors.get(color, colors["Default"]))]
    for key, modifier in {
        1: kwargs.get('bold', False),
        2: kwargs.get('dim', False),
        4: kwargs.get('underlined', False),
        5: kwargs.get('blink', False),
        7: kwargs.get('reverse', False),
        8: kwargs.get('hidden', False),
        }.items():
        if modifier:
            attrs.append(str(key))
    return '\x1b[%sm%s\x1b[0m' % (';'.join(attrs), string)

if __name__ == '__main__':

    print(highlight("default", "error"))
    print(highlight("red", "Red"))
    print(highlight("Xmas tree", "Green", bold=True, blink=True, reverse=True, underlined=True))
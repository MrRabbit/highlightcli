# highlightcli

Format a string with terminal color and style.

Source: https://misc.flogisoft.com/bash/tip_colors_and_formatting

This module provides *some* terminal highlighting functionalities.

## Install

- install this folder in your PYTHONPATH
- use with ```from highlightcli import highlight```

## Example

```python
from highlightcli import highlight

print(highlight("default", "error"))
print(highlight("red", "Red"))
print(highlight("Xmas tree", "Green", bold=True, blink=True, reverse=True, underlined=True))
```
